var menuApp = angular.module('menuApp', []);
menuApp.directive('ipsmenu', function($http, $rootScope) {
	return {
		restrict: 'E' ,
		templateUrl:"shared/lookandfeel/menu/menuView.html",
		link: function(scope, elem, attr) {
			$http.get(attr["xmlsource"]).then(function(response) {
				$rootScope.menuData = JSON.parse(xml2json(response.data,""));
			});
			
			scope.processAction = function(action){
				$rootScope.menuAction = action;
				$rootScope.menuItemSelectHandler(action);
			};
		}
	};
});