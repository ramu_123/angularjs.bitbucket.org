(function (){
  'use strict';
  	angular.module('headerApp').controller('HeaderController',HeaderController);
	function HeaderController($scope, $rootScope, $http, $location, $q, HeaderConstantService) {
		//$scope.applicationName = HeaderConstantService._IPS_APP_NAME;
		if(!angular.isDefined($rootScope.userProfile)){
			//	$location.path(HeaderConstantService._LOGIN_PATH);
		}

		$rootScope.loadUserProfile = function(userLanName){
			var deferred = $q.defer();
			HeaderConstantService._ACTION_TYPE_GET_USER_PROFILE['lanName']=userLanName; 		
	        $http(
            	 {
                   method : HeaderConstantService._POST_METHOD,
                   headers: HeaderConstantService._HEADER_INFO,
                   url : HeaderConstantService._URL_TO_POST,
                   data: HeaderConstantService._ACTION_TYPE_GET_USER_PROFILE
                 }
	          )
	          .success(function(responseData) {
	        	  if(responseData[HeaderConstantService._GET_USER_PROFILE] != null && responseData[HeaderConstantService._GET_USER_PROFILE] != "undefined"){
	        		  deferred.resolve(responseData[HeaderConstantService._GET_USER_PROFILE]);
	        	  }else{
	        		  deferred.reject('Internal Server Error, Resource not available.');
	        	  }
	          })
	          .error(function(data, status, headers, config) {
	        	  deferred.reject('Network Error, Code:' + status);
	          });
	          return deferred.promise;
	    };
	    
	    $scope.loadjscssfile = function(cssName){
	    	var fileName = 'shared/lookandfeel/header/assets/css/' + cssName +'.css';
			var fileref=document.createElement("link");
			  fileref.setAttribute("rel", "stylesheet");
			  fileref.setAttribute("type", "text/css");
			  fileref.setAttribute("href", fileName);
			  if (typeof fileref!="undefined"){
				  document.getElementsByTagName("head")[0].appendChild(fileref);
			  }
	    };
	    
	    $scope.loadjscssfile(HeaderConstantService._DEFAULT_COLOR);
	    
		$scope.resizeText = function(multiplier) {
			
			if (document.body.style.fontSize == "") {
			    document.body.style.fontSize = HeaderConstantService._FONT_SIZE_UNIT;
			}
			var startFontSize=parseFloat(document.body.style.fontSize);
			
			if(multiplier ==0){
				document.body.style.fontSize = HeaderConstantService._FONT_SIZE_UNIT;
				$scope.loadjscssfile(HeaderConstantService._DEFAULT_COLOR);
				return;
			}else if(multiplier == 1 && startFontSize<=1.4){
				document.body.style.fontSize = parseFloat(document.body.style.fontSize) + (multiplier * 0.2) + "em";
			}else if(multiplier == -1 && startFontSize>=0.6){
				document.body.style.fontSize = parseFloat(document.body.style.fontSize) + (multiplier * 0.2) + "em";
			}			
	    };			
	}
})();