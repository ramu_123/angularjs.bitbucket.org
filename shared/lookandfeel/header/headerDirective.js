'use strict';
angular.module('headerApp', []).directive('ipsheader', function($http, $rootScope,HeaderConstantService) {
	return {
		restrict: 'E' ,
		templateUrl:HeaderConstantService._HEADER_VIEW_TEMPLATE,
		link: function(scope, elem, attr) {
			HeaderConstantService._ACTION_TYPE_GET_USER_PROFILE['lanName'] = attr["lanname"]; 
			$http(
	            	 {
	                   method : HeaderConstantService._POST_METHOD,
	                   headers: HeaderConstantService._HEADER_INFO,
	                   url : HeaderConstantService._URL_TO_POST,
	                   data: HeaderConstantService._ACTION_TYPE_GET_USER_PROFILE
	                 }
		          )
		          .success(function(responseData) {
		        	  $rootScope.userProfile = scope.userProfile = responseData[HeaderConstantService._GET_USER_PROFILE];
		        	  //$rootScope.maxActiveSecs = responseData[HeaderConstantService._ACTION_TYPE_GET_SESSION_MAX_INACTIVE_INTERVAL];
		        	  //$rootScope.httpSessionID = responseData[HeaderConstantService._ACTION_TYPE_GET_SESSION_ID];
		          })
		          .error(function(data, status, headers, config) {
		        	  alert(status);
		          });			
		}
	};
});