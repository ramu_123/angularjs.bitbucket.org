/**
 * 
 */
'use strict';
angular.module('ui-util').factory('IPSHttpService', function ($http,$q) {
	var _GET_METHOD='GET';
	var _POST_METHOD='POST';    
    var _HEADER_INFO={'Content-Type': 'application/json'};
       
   var post = function(url_to_post,inputData){
		var deferred = $q.defer();		 		
        $http(
        	 {
        		 method : _POST_METHOD,
 	            headers: _HEADER_INFO,
 	            url : url_to_post,
 	            data: inputData
             }
          )
          .success(function(responseData) {        	 
        		  deferred.resolve(responseData);     
        		 // alert('responseData:'+responseData);
          })
          .error(function(data, status, headers, config) {
        	  deferred.reject('Error:' + status);
        	  alert('Error:'+status);
          });
          return deferred.promise;
    };
    var get = function(url_to_get){
		var deferred = $q.defer();		 		
        $http(
        	 {
        		method : _GET_METHOD,
 	            headers: _HEADER_INFO,
 	            url : url_to_get
             }
          )
          .success(function(responseData) {        	 
        		  deferred.resolve(responseData);        	 
          })
          .error(function(data, status, headers, config) {
        	  deferred.reject('Error:' + status);
          });
          return deferred.promise;
    };
    
    return{
    	post:post,
    	get:get
    };
  
/**    SAMPLE CODE TO INVOKE HTTP SERVICE METHODS FROM CONTROLLER
     //var testPromise=IPSHttpService.post(LoginConstantService._URL_TO_POST,LoginConstantService._ACTION_TYPE_GET_SUBSYSTEM_URLS);
     var testPromise=IPSHttpService.post('jsonservlet',{actionType:"GET_SUBSYSTEM_URLS"});
    testPromise.then(
    		function(resolve){
    			var testResponseObj=resolve;
            }, 
            function(reject){
            	alert('reject');
            }
        );
 */   
});