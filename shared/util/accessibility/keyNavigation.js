/**
 * 
 */
'use strict';
angular.module('ui-util').directive('keyNavigation', function () {
    return function (scope, element, attrs) {  
    	element.attr("tabIndex",0);
    	element.on('focus',function(){
        	element.css("border","2px solid #000");
        	element.css("border-color","#FB9605");
    	});
    	element.on('blur',function(){
        	element.css("border","");
        	element.css("border-color","");
    	});
    };
});