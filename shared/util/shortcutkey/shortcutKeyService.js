/**
 * 
 */
(function (){
  'use strict';
  
angular.module('ui-util').service('ShortcutKeyService', function($rootScope){
    
	var allowInElements= ['INPUT', 'SELECT', 'TEXTAREA'];

	///IPS ADMIN MENU
    this.setKeysForAdminMenu = function(hotkeys,HomeConstantService) {
    	hotkeys.add({
	        combo: 'ctrl+alt+o',
	        description: 'Home',
	        allowIn:allowInElements,
	        callback: function() {		        	
	        	$rootScope.menuAction=HomeConstantService._HOME_PARTIAL;
	        }
	      });
	    hotkeys.add({
	        combo: 'ctrl+alt+a',
	        description: 'IPS Assignment',
	        allowIn:allowInElements,
	        callback: function() {	         
	        	$rootScope.menuAction=HomeConstantService._IPS_ASSIGNMENT_PARTIAL;
	        }
	      });
	    hotkeys.add({
	        combo: 'ctrl+alt+u',
	        description: 'User Management',
	        allowIn:allowInElements,
	        callback: function() {
	        	$rootScope.menuAction=HomeConstantService._USER_MANGMENT_PARTIAL;
	        }
	      });
	    hotkeys.add({
	        combo: 'ctrl+alt+s',
	        description: 'Save',
	        allowIn:allowInElements,
	        callback: function() {		        	
	        	$rootScope.menuAction=HomeConstantService._SAVE_ACTION;
	        }
	      });
	    hotkeys.add({
	        combo: 'ctrl+alt+e',
	        description: 'Exit',
	        allowIn:allowInElements,
	        callback: function() {	          
	        	$rootScope.menuAction=HomeConstantService._EXIT_PARTIAL;
	        }
	      });
    };
    //IPS USER MANAGEMENT SCREEN
    this.setKeysForUser = function(hotkeys) {
    	hotkeys.add({
	        combo: 'ctrl+alt+f',
	        description: 'Filter in User Management',
	        callback: function() {
	        	     	
	        }
	      });
	    hotkeys.add({
	        combo: 'ctrl+alt+g',
	        description: 'Get All Users in User Management',
	        callback: function() {	      
	        	
	        }
	      });	    
    };
  
}); 
})();