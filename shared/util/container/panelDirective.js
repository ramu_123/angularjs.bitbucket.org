'use strict';
angular.module('ui-util', []).directive('panel', function($window) {
	return {
	    restrict: "E",
	    transclude: true,
	    scope: {
	      title: "@"
	    },
	    template: "<div style='border: 1px solid #000000; width:97%' align='left'>" +
	              "<h3>{{title}}</h3>" +
	              "<div ng-transclude></div></div>"
	  };
});