/**
 * 
 */
'use strict';
angular.module('ui-util').directive('focusableLabel', function() {	
	
	var link = function (scope, element, attrs) {
		element.attr("tabIndex",0);
    	element.on('focus',function(){
        	element.css("border","3px solid #000");
        	element.css("border-color","#FB9605");
    	});
    	element.on('blur',function(){
        	element.css("border","");
        	element.css("border-color","");
    	});
	};

	  return {
		template: '<span>{{label}}</span>',
		restrict: 'E',	
		replace: true,
	    scope: {
	        label: '@'
	      },
	    link: link

	  };
	  
	  /**
	   * SAMPLE CODE TO USE focusableLabel.
	   * <focusable-label label='{{userProfile.lanName}}'/>
	   **/
});