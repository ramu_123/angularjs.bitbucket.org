var url;
var windowname;
var windowparams;
var moveX;
var moveY;
var resizeX;
var resizeY;
var popupWins = new Array();
var subSystemCode, userLanName, selectedRoleCode = "#";
var subSystems;
var validNavigation = false;

var activeFlag = 'N';
var z;
var x;
var jsTimeoutTemp;
var contextpath;
var activeSecsConstant = 60;
var activeSecs = activeSecsConstant;
var httpsessionid;

/***************************************************************************************************** 
the popupWins array stores an object reference for each separate window that is called, based upon
the name attribute that is supplied as an argument
*****************************************************************************************************/
function windowOpener(url, windowname, windowparams,moveX,moveY,resizeX,resizeY) {
	if ( typeof( popupWins[windowname] ) != "object" ){
		popupWins[windowname] = window.open(url,windowname,windowparams);
	} else {
		if (!popupWins[windowname].closed){
			popupWins[windowname].location.href = url;
		} else {
			popupWins[windowname] = window.open(url, windowname,windowparams);
		}
	}
	popupWins[windowname].focus();
	try{
		popupWins[windowname].resizeTo(resizeX,resizeY);
		setTimeout(popupWins[windowname].moveTo(moveX,moveY),1000);
	}catch(e){
		
	}
	return false;
}

/***************************************************************************************************** 
Subsystems calls these methods to get subsystem, lanname, selected role code  
*****************************************************************************************************/
function getSubSystemCode(){
	return subSystemCode;
}
function getUserLanName(){
	return userLanName;
}
function getSelectedRoleCode(){
	return selectedRoleCode;
}
function setActiveFlag(){
	angular.element(document.getElementById('homeControllerDiv')).scope().extendSession();
}

/***************************************************************************************************** 
For a list of events that triggers onbeforeunload on IE  
*****************************************************************************************************/
window.onbeforeunload = function() { 
	if (!validNavigation) { 
		closePopupWindows('notlogout');    
	}   
}; 

/***************************************************************************************************** 
For a list of events that triggers onbeforeunload on IE  
*****************************************************************************************************/
function closePopupWindows(pwindow){
	if(subSystems != null && subSystems.length > 0){
		var confirmValue = false;
		if(pwindow =='logout'){
			var openWindowCount = 0;
			for(var i = 0; i < subSystems.length; i++){
				if( typeof( popupWins[subSystems[i].subSystemCode] ) != "undefined" ) {
					if( !popupWins[subSystems[i].subSystemCode].closed) {
						openWindowCount++;
					}
				}
			}
			if(openWindowCount > 0)confirmValue = confirmBox();
		}
		for(var i = 0; i < subSystems.length; i++){
			if( typeof( popupWins[subSystems[i].subSystemCode] ) != "undefined" ) {   
				if( popupWins[subSystems[i].subSystemCode].closed) {
				} else {
					if(pwindow =='logout'){
						if(confirmValue){
							popupWins[subSystems[i].subSystemCode].close();
						} else {
							return false;
						}
					}else {
						popupWins[subSystems[i].subSystemCode].close();
					}    			 
				}    		  	    	  
			}
		}
	}
	return true;
}
function confirmBox(){  		
	var r=confirm("You have opened application(s). Do you want to logout?");
	return r;  		
};

/***************************************************************************************************** 
This method is used by Admin LoginController to start countdown for session timeout.  
*****************************************************************************************************/
function doTimeOut(){
	countdown();
}

function countdown() {
	z = setTimeout('displayWarning()',((jsTimeoutTemp - 300)*1000));
}

function displayWarning() {
	window.clearTimeout(z);
	angular.element(document.getElementById('homeControllerDiv')).scope().displayWarning();
}

function setNewTime(){
	window.clearTimeout(z);
	countdown();
}

/***************************************************************************************************** 
This method Returns window object if the window is not closed.  
*****************************************************************************************************/
function getSubsytemWindow(_subsystemCode){
	if( typeof( popupWins[_subsystemCode] ) != "undefined" && !popupWins[_subsystemCode].closed) { 
		return popupWins[_subsystemCode];
	}
	return null;
}