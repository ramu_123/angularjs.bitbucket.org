(function (){
'use strict';
angular.module('IPSAdminApp').controller('LoginController',LoginController);
LoginController.$inject=['$scope','$location','$rootScope','$routeParams','$http','$window','$q','LoginConstantService'];
/*
This is called once the route's view is defined as LoginController. This is called for /login, /login/pathname/lanname, /Gateway, /SSOGateway.
This controller defines following methods:
1. 'changeView' method: This method will be called only when user enter username and click button to login. This method expects user validity flag in responseData.
Once the user is valid, it closes current window and opens new window with full screen.

Following uRL directly routes to admin home and loads jayavel_a's profile.
index.html#/login/adminHome/jayavel_a

*/
function LoginController($scope, $location, $rootScope, $routeParams, $http, $window,$q,LoginConstantService) {
	$rootScope.applicationName = LoginConstantService._IPS_APP_NAME;
	$scope.userProfile = LoginConstantService._USER_PROFILE;
	$scope.changeView = function(view){
		LoginConstantService._ACTION_CHECK_USER['lanName']=$scope.userProfile.lanName; 
		$http(
	           	 {
	                  method : LoginConstantService._POST_METHOD,
	                  headers: LoginConstantService._HEADER_INFO,
	                  url : LoginConstantService._URL_TO_POST,
	                  data: LoginConstantService._ACTION_CHECK_USER
	                }
		          )
		          .success(function(responseData) {
		      	    	if(responseData[LoginConstantService._GET_CHECK_USER]){
				      	    try {
				    	    	var nWindow= window.open($location.absUrl() + LoginConstantService._HOME_PARTIAL_PATH + "/"+$scope.userProfile.lanName + "/", 'test', 'status=yes,resizable=yes,copyhistory=no,scrollbars=1');
				    	        nWindow.moveTo(2,2);
				    	        nWindow.resizeTo(screen.availWidth-10,screen.availHeight-10);
				    	        if (window.focus) nWindow.focus();  
				    	    }catch(e){}
				    	    window.open('', '_self', '');
				    	 	window.close();		      	    		
		      	    	}else{
		      	    		alert(LoginConstantService._NO_USER_ERR_MGS);
		      	    	}
		          })
		          .error(function(data, status, headers, config) {
		        	  alert(status);
		          });
	};  
    $scope.getAllSubsystem = function() {
    	var deferred = $q.defer();
        $http(
           	 {
                  method : LoginConstantService._POST_METHOD,
                  headers: LoginConstantService._HEADER_INFO,
                  url : LoginConstantService._URL_TO_POST,
                  data: LoginConstantService._ACTION_TYPE_GET_SUBSYSTEM_URLS
                }
	          )
	          .success(function(responseData) {
	        	  //jsTimeoutTemp = responseData[LoginConstantService._ACTION_TYPE_GET_SESSION_MAX_INACTIVE_INTERVAL];	     
	        	  //doTimeOut();
	        	  //$scope.prepareSubsystemData(responseData[LoginConstantService._GET_ALL_SUB_SYSTEM]);
	        	  deferred.resolve(responseData);
	          })
	          .error(function(data, status, headers, config) {
	        	  deferred.reject('Network Error, Code:' + status);
	          });
        return deferred.promise;
    };
    $scope.prepareSubsystemData = function(subsystemData) {
    	$rootScope.subSystems = [];
		angular.forEach(subsystemData, function(value, key) {
			if(value._subsystemCode == "ADMIN")
				this.push({subSystemCode:value._subsystemCode, subSystemDesc:value._displayName, enableDefault:"false", callBackFn:"", subSystemModal:"#businessRoleModal", subSystemWebUrl:"http://localhost:7001/IPSAdminWeb/index.html#/GateWay/adminHome", displayOrder:value._displayOrder});
			else
			this.push({subSystemCode:value._subsystemCode, subSystemDesc:value._displayName, enableDefault:"false", callBackFn:"", subSystemModal:"#businessRoleModal", subSystemWebUrl:value._webUrl, displayOrder:value._displayOrder});
		}, $rootScope.subSystems);
		$rootScope.subSystems.push({subSystemCode:"BUILD", subSystemDesc:"Build Information", enableDefault:"true", callBackFn:"this.showBuildInfo()", subSystemModal:"#buildModal", subSystemWebUrl:"",displayOrder:$rootScope.subSystems.length});
		subSystems = $rootScope.subSystems;
    };
    /*
    Following block of code will be called every time when controller is called.
    ------START------
    */
    if($routeParams.pathName != null){
    	$scope.userProfilePromise = null;
    	if($routeParams.lanName != null){
    		$scope.subsystemPromise = $scope.getAllSubsystem();
    		$scope.subsystemPromise.then(
    	    		function(resolve){
    	    			jsTimeoutTemp = resolve[LoginConstantService._ACTION_TYPE_GET_SESSION_MAX_INACTIVE_INTERVAL];	     
    		        	doTimeOut();
    		        	$scope.prepareSubsystemData(resolve[LoginConstantService._GET_ALL_SUB_SYSTEM]);
    	            }, 
    	            function(reject){
    	            	$rootScope.errorMsg = reject;
    	            	$location.path(LoginConstantService._ERROR_PARTIAL_PATH);
    	            }
    	        );
    		$scope.userProfilePromise = $rootScope.loadUserProfile($routeParams.lanName);
    	}else{
    		try{
    			$scope.userProfilePromise = $rootScope.loadUserProfile(window.opener.getUserLanName());
    	    }catch(e){
    	    }
    	}
    	if($routeParams.pathName == LoginConstantService._ADMIN_HOME_PARTIAL_PATH){
    		$rootScope.applicationName = LoginConstantService._ADMIN_APP_TITLE;
    		angular.element($window).bind('click', function(){window.opener.setActiveFlag();});
    		$window.document.title = LoginConstantService._ADMIN_APP_TITLE;
    	}else if($routeParams.pathName == LoginConstantService._HOME_PARTIAL_PATH){
			$scope.userProfile.lanName = $routeParams.lanName;
    		$scope.changeView(null);				
        }else{
    		angular.element($window).bind('click', function(){setActiveFlag();});
    	}
    	$scope.userProfilePromise.then(
    		function(resolve){
    			if(resolve != null){
    				setTimeout(function () {
    				    $scope.$apply(function () {
    				    	$rootScope.userProfile = resolve;
    				    	$location.path($routeParams.pathName);
    				    });
    				}, 500); 
    			}else{
                	$rootScope.errorMsg = LoginConstantService._SERVER_ERR_MGS;
                	$location.path(LoginConstantService._ERROR_PARTIAL_PATH);
    			}
            }, 
            function(reject){
            	$rootScope.errorMsg = reject;
            	$location.path(LoginConstantService._ERROR_PARTIAL_PATH);
            }
        );
    }else if($routeParams.ssoToken != null){
		$scope.ssoTokenCheckWSPromise = null;
        try{
    			$scope.ssoTokenCheckWSPromise = $scope.wsCall4TokenCheck($routeParams.ssoToken);
   	    }catch(e){
    	}
		$scope.ssoTokenCheckWSPromise.then(
    		function(resolve){
    			if(resolve != null){
                     $scope.userProfile.lanName = resolve["user_lan_name"];//successful resoved results should contain user lan name.
    				 $scope.changeView(null);
    			}else{
                	$rootScope.errorMsg = "User not allowed to access this resource";
                	$location.path(LoginConstantService._ERROR_PARTIAL_PATH);
    			}
            }, 
            function(reject){
            	$rootScope.errorMsg = reject;
            	$location.path(LoginConstantService._ERROR_PARTIAL_PATH);
            }
        );		
	}
/*------END------*/    
}
})();