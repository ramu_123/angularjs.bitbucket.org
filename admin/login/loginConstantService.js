angular.module('IPSAdminApp').constant('LoginConstantService',(function() {
	var _POST_METHOD='POST';
    var _URL_TO_POST='jsonservlet';
    var _HEADER_INFO={'Content-Type': 'application/json'};
    var _ACTION_TYPE_GET_SUBSYSTEM_URLS={actionType:"GET_SUBSYSTEM_URLS"};
    var _GET_ALL_SUB_SYSTEM="GET_ALL_SUB_SYSTEM";
    var _GET_SUBSYSTEM_URLS="GET_SUBSYSTEM_URLS";
    var _ACTION_CHECK_USER={lanName:'',actionType:"GET_CHECK_USER"};
    var _GET_CHECK_USER = "GET_CHECK_USER";
	var _ACTION_TYPE_GET_SESSION_ID="GET_SESSION_ID";
	var _ACTION_TYPE_GET_SESSION_MAX_INACTIVE_INTERVAL="GET_SESSION_MAX_INACTIVE_INTERVAL";
    var _HOME_PARTIAL_PATH = '/home';
    var _ERROR_PARTIAL_PATH = '/error';
    var _USER_PROFILE = {lanName:""};
    var _NO_USER_ERR_MGS = "User does not have permission to use IPS application.";
    var _SERVER_ERR_MGS = "Internal server error, please try again.";
    var _ADMIN_APP_TITLE = "IPS Administration";
    var _IPS_APP_NAME = "Industrial Prices Systems";
    var _ADMIN_HOME_PARTIAL_PATH = 'adminHome';
	return {
    	_POST_METHOD: _POST_METHOD,
    	_URL_TO_POST: _URL_TO_POST,
    	_HEADER_INFO: _HEADER_INFO,
    	_ACTION_TYPE_GET_SUBSYSTEM_URLS:_ACTION_TYPE_GET_SUBSYSTEM_URLS,
    	_HOME_PARTIAL_PATH:_HOME_PARTIAL_PATH,
    	_USER_PROFILE:_USER_PROFILE,
    	_ACTION_CHECK_USER:_ACTION_CHECK_USER,
    	_GET_CHECK_USER:_GET_CHECK_USER,
    	_NO_USER_ERR_MGS:_NO_USER_ERR_MGS,
    	_ADMIN_APP_TITLE:_ADMIN_APP_TITLE,
    	_ADMIN_HOME_PARTIAL_PATH:_ADMIN_HOME_PARTIAL_PATH,
    	_IPS_APP_NAME:_IPS_APP_NAME,
    	_ACTION_TYPE_GET_SESSION_ID:_ACTION_TYPE_GET_SESSION_ID,
    	_ACTION_TYPE_GET_SESSION_MAX_INACTIVE_INTERVAL:_ACTION_TYPE_GET_SESSION_MAX_INACTIVE_INTERVAL,
    	_GET_SUBSYSTEM_URLS:_GET_SUBSYSTEM_URLS,
    	_ERROR_PARTIAL_PATH:_ERROR_PARTIAL_PATH,
    	_GET_ALL_SUB_SYSTEM:_GET_ALL_SUB_SYSTEM,
    	_SERVER_ERR_MGS:_SERVER_ERR_MGS
    };
})());