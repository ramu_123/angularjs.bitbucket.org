(function (){
  'use strict';
  
angular.module('IPSAdminApp').controller('IPSUserController',IPSUserController);

IPSUserController.$inject=['$scope','$http','$filter','IPSUserValueService','IPSUserConstantService'];

//IPSUserController.$inject=['$filter','IPSUserValueService','IPSUserConstantService'];

function IPSUserController ($scope,$http,$filter,IPSUserValueService,IPSUserConstantService) {
//function IPSUserController (IPSUserValueService,IPSUserConstantService) {
	$scope.filterColumns =IPSUserConstantService._IPS_USER_VIEW_GRID_FILTER_COLUMNS;
	$scope.ipsUserList=IPSUserValueService.getIPSUserList();
	$scope.colDefs=IPSUserConstantService._IPS_USER_VIEW_GRID_COL_DEFS; 
 	$scope.userListSize=IPSUserValueService.getIPSUserListSize();
 	$scope.selectedIPSUser=IPSUserValueService.getSelectedIPSUser();

 	$scope.pagingOptions= {
	          pageSizes: [5,15,30],
	          pageSize: 30,
	          currentPage: 1
	  };	
	$scope.filterOptions = {
			    filterText: "",
			    filterColumn: "",
			    useExternalFilter: true
	}; 
	
	$scope.setIPSUserList = function(totalServerList,listSize){
		IPSUserValueService.setIPSUserList(totalServerList,listSize);
		$scope.ipsUserList = IPSUserValueService.getIPSUserList();
		$scope.userListSize=listSize;
	};

	$scope.getIPSUserList = function(){
		$scope.ipsUserList=IPSUserValueService.getIPSUserList();
		return $scope.ipsUserList;
	};
	
	$scope.selectedIPSUser = function(){
	   IPSUserValueService.setIPSUserList($scope.selectedIPSUser);
	};

	
	$scope.setPagingData = function(data, page, pageSize){	
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.setIPSUserList(pagedData,data.length);
        if (!$scope.$$phase) {
        	$scope.$apply();
        }
    };

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
        	$scope.getAllIPSUser($scope.pagingOptions.pageSize,$scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    
    $scope.$watch('fileterOptions', function (newVal, oldVal) {
    	 if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
    		 $scope.getAllIPSUser($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope._filterOptions.filterText,$scope._filterOptions.filterColumn);
        }
    }, true);
    

    $scope.filterResultData=function(){
	  var gcFilterObject ={};
	  gcFilterObject[$scope.filterOptions.filterColumn.field]=$scope.filterOptions.filterText; 
      var filterResult = $filter(IPSUserConstantService._FILTER_PARAM)(IPSUserValueService.getIPSUserList(), gcFilterObject);
      if(filterResult.length>0){
        $scope.setIPSUserList(filterResult,IPSUserValueService.getIPSUserListSize());
      }
    };

    $scope.getAllIPSUser= function(pageSize, page, searchText,searchColumn) {
    	$scope.promise = $http(
     	 {
            method : IPSUserConstantService._POST_METHOD,
            headers: IPSUserConstantService._HEADER_INFO,
            url : IPSUserConstantService._URL_TO_POST,
            data: IPSUserConstantService._REQUEST_DATA
          }
         ).success(function(responseData) {
        	var totalServerList = angular.fromJson(responseData);
        	$scope.setIPSUserList(totalServerList);
            if(angular.isDefined($scope.filterOptions.filterColumn.field)&& angular.isDefined($scope.filterOptions.filterText)){
               if($scope.filterOptions.filterText.length>0){
            	   $scope.filterResultData();
               }
            }
            $scope.setPagingData($scope.getIPSUserList(),$scope.pagingOptions.currentPage,$scope.pagingOptions.pageSize);
           }
         ).error(function(data, status, headers, config) {
         	   //alert(status);
           });
      };
      $scope.gridOptions = { 
              data: 'ipsUserList',
              columnDefs:'colDefs',
              enableColumnResize: true,
              filterOptions: $scope.filterOptions,
              pagingOptions:$scope.pagingOptions,
              totalServerItems: 'userListSize',
              displaySelectionCheckbox: false,
              enableCellSelection: true,
              canSelectRows: false,
              enablePaging: true,
              multiSelect:false,
              showFilter:true,
              selectedItems:IPSUserValueService._selectedIPSUser,
      		  showFooter: true
         };
      
      $scope.businessRoleChanged = function(col,row) {
  	    //alert("BR object"+row.entity.myEditableProperty.businessRole);
  	    //alert("selected br="+row.entity.myEditableProperty.businessRole._businessRoleCode);
  	    //alert("Index"+row.rowIndex);
      };
      /*$scope.$on("$destroy", function() {
          alert("Destroy Called!");
      });*/
};
})();