(function (){
  'use strict';
  
angular.module('IPSAdminApp').service('IPSUserValueService',IPSUserValueService);

function IPSUserValueService(){

  var _ipsUserList=[];

  var _ipsUserListSize=0;

  var _selectedIPSUser=[];

  var addIPSUser = function(newObj) {
	  _ipsUserList.push(newObj);
  };

  var getIPSUserList = function(){
      return _ipsUserList;
  };

  var getIPSUserListSize = function() {
	  return _ipsUserListSize;
  };

  var setIPSUserList = function(newList,size) {
	  if(angular.isDefined(newList)==true){
		_ipsUserList =newList;
		_ipsUserListSize=size;		
      }
  };

  var setSelectedIPSUser = function(selectedUserObj) {
	  if(angular.isDefined(selectedUserObj)==true){	  
	    _selectedIPSUser=selectedUserObj;
	  }
  };
  
  var getSelectedIPSUser = function() {
	 return _selectedIPSUser;
  };

  
  return {
		 addIPSUser:addIPSUser,
		 setSelectedIPSUser:setSelectedIPSUser,
		 _ipsUserListSize:_ipsUserListSize,
		 getIPSUserList:getIPSUserList,
		 setIPSUserList:setIPSUserList,
		 getIPSUserListSize:getIPSUserListSize,
		 getSelectedIPSUser:getSelectedIPSUser
	   };

  
};
 
})();