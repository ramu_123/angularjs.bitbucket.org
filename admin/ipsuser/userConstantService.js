
angular.module('IPSAdminApp').constant('IPSUserConstantService',(function() {
	var _POST_METHOD='POST';
    var _URL_TO_POST='jsonservlet';
    var _HEADER_INFO={'Content-Type': 'application/json'};
    var _REQUEST_DATA={actionType:"GET_ALL_IPS_USERS"};
    
/*    $scope.postMethod='POST';
    $scope.urlToPost='jsonservlet';
    $scope.headerInfo={'Content-Type': 'application/json'};
    $scope.requestData={actionType:"GET_ALL_IPS_USERS"};
  */  
    
    var _FILTER_OPTIONS='filterOptions';
    var _PAGING_OPTIONS='pagingOptions';
    var _FILTER_PARAM='filter';
    
    var _LABEL_CELL_TEMPLATE='<div class="ngCellText colt{{$index}}" key-Navigation>{{row.getProperty(col.field)}}</div>';
    
    var _BUSINESS_ROLE_CELL_TEMPLATE='<div key-Navigation><select ng-hide="row.entity._businessRoles.length <= 0" '+
                                     'ng-init="row.entity.myEditableProperty.businessRole= row.entity._businessRoles[0]" '+
                                     ' ng-model="row.entity.myEditableProperty.businessRole" '+
                                     'ng-options="businessRole._businessRoleDesc for businessRole in row.entity._businessRoles" '+
                                     'ng-change=\"businessRoleChanged(col, row)\"/ '+
                                     '></select></div>';
    var _TASK_CELL_TEMPLATE='<div key-Navigation><select  '+
                            'ng-hide="row.entity._businessRoles.length <= 0" '+
                            'ng-init="row.entity.myEditableProperty.task = row.entity._businessRoles[0]._tasks[0]" '+
                            'ng-model="row.entity.myEditableProperty.task" '+
                            'ng-options="task._taskSubSystem for task in row.entity.myEditableProperty.businessRole._tasks"></select></div>';
    
    var _ACTIVE_YN_CELL_TEMPLATE='<div key-Navigation style="text-align:center;" class="ngCellText"><input type="checkbox" ng-init="row.entity.activeYN" ng-model="row.entity.activeYN" ></div>';

    var _TEXTBOX_CELL_TEMPLATE='<div key-Navigation style="text-align:center;" class="ngCellText"><input key-Navigation type="text" ng-init=\"COL_FIELD\" ng-model=\"COL_FIELD\" ></div>';
    
    var _IPS_USER_VIEW_GRID_COL_DEFS=[
                    {field: 'userFirstName', displayName: 'First Name',width: "8%",cellTemplate:_TEXTBOX_CELL_TEMPLATE},
                    {field: 'userLastName', displayName: 'Last Name',width: "8%",cellTemplate:_TEXTBOX_CELL_TEMPLATE},                     
                    {field: 'userLanName', displayName: 'Lan Name',width: "8%",cellTemplate:_LABEL_CELL_TEMPLATE},
                    {field: 'userEmailAddress', displayName: 'Email', width: "14%",cellTemplate:_LABEL_CELL_TEMPLATE},
                    {field: 'activeYN', displayName: 'Active', cellTemplate: _ACTIVE_YN_CELL_TEMPLATE,width: "3%"},
                    {field: 'programTypeCode', displayName: 'Program',width: "4%",cellTemplate:_LABEL_CELL_TEMPLATE},
                    {field: 'businessRoles', displayName: 'Business Roles',  cellTemplate:_BUSINESS_ROLE_CELL_TEMPLATE,cellEditableCondition: 'true',width: "18%"},
                    {field: 'tasks', displayName: 'Tasks', cellTemplate:_TASK_CELL_TEMPLATE,enableCellSelection:true,cellEditableCondition:'true',width: "37%"}
                   ];

    
    var _IPS_USER_VIEW_GRID_FILTER_COLUMNS= [
                                      {name:'',field: ''},
                                      {name:'First Name',field: 'userFirstName'},
                                      {name:'Last Name',field: 'userLastName'},
                                      {name:'Lan Name',field: 'userLanName'},
                                      {name:'Email',field: 'userEmailAddress'},
                                      {name:'Active',field: 'activeYN'},
                                      {name:'Program',field: 'programTypeCode'},
                                      {name:'User SID',field: 'userSid'}
                                    ];
    return {
    	_POST_METHOD: _POST_METHOD,
    	_URL_TO_POST: _URL_TO_POST,
    	_HEADER_INFO: _HEADER_INFO,
    	_REQUEST_DATA:_REQUEST_DATA,
    	_BUSINESS_ROLE_CELL_TEMPLATE:_BUSINESS_ROLE_CELL_TEMPLATE,
    	_TASK_CELL_TEMPLATE:_TASK_CELL_TEMPLATE,
    	_ACTIVE_YN_CELL_TEMPLATE:_ACTIVE_YN_CELL_TEMPLATE,
    	_PAGING_OPTIONS:_PAGING_OPTIONS,
    	_IPS_USER_VIEW_GRID_COL_DEFS:_IPS_USER_VIEW_GRID_COL_DEFS,
    	_FILTER_OPTIONS:_FILTER_OPTIONS,
    	_PAGING_OPTIONS:_PAGING_OPTIONS,
    	_FILTER_PARAM:_FILTER_PARAM,
    	_IPS_USER_VIEW_GRID_FILTER_COLUMNS:_IPS_USER_VIEW_GRID_FILTER_COLUMNS
    	
    };
})());
