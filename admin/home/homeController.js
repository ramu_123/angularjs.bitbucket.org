(function (){
'use strict';
	angular.module('IPSAdminApp').controller('HomeController',HomeController);
	HomeController.$inject=['$scope','$rootScope','$http','$location','$routeParams','$window','$timeout','HomeConstantService','hotkeys','ShortcutKeyService'];

	function HomeController($scope,$rootScope,$http,$location, $routeParams,$window, $timeout,HomeConstantService,hotkeys,ShortcutKeyService) {
		//$scope.subSystems = subSystems = HomeConstantService._IPS_SUB_SYSTEMS;
		$scope.counter = HomeConstantService._COUNTDOWN_TIMER_MIN * 60;
		$scope.sessionTimeout = null;
		$scope.manualLogoutFlag = false;
		$scope.selectSubSystem = function(subSysObj) {
			if(subSysObj.callBackFn != "")eval(subSysObj.callBackFn);
	        $scope.selectedBusinessRoleTaskPermList = [];
	        $scope.selectedBusinessRoleTaskPermList.push(HomeConstantService._DEFAULT_ROLE_OPTION);
	        angular.forEach($scope.userProfile.businessRoleTaskPermissionMap, function(businessRoleTaskPerms, taskCode) {
	        	angular.forEach(businessRoleTaskPerms, function(taskPerm){
	        		if(subSysObj.subSystemCode == taskPerm.subSystem){
	        			if($.grep($scope.selectedBusinessRoleTaskPermList, function(e) { return e.roleCodeDesc == taskPerm.businessRoleCodeDesc; }) == ""){
	        	        	$scope.selectedBusinessRoleTaskPermList.push({roleCode:taskPerm.businessRoleCode, roleCodeDesc:taskPerm.businessRoleCodeDesc, subSystem:subSysObj.subSystemCode, subSystemWebUrl:subSysObj.subSystemWebUrl});
	        	        }
	        		}
	        	});
	          });
	        $scope.selectedBusinessRoleTaskPermList.sort();
	        var subsystemWindow = getSubsytemWindow(subSysObj.subSystemCode);
	        $scope.selectedItem = HomeConstantService._DEFAULT_ROLE_OPTION;
	        if(subsystemWindow != null){
	        	subSysObj.subSystemModal = '';
	        	$scope.selectedBusinessRoleTaskPermList = null;
	        	subsystemWindow.focus();
	        }else{
		        if($scope.selectedBusinessRoleTaskPermList.length == 2){
		        	subSysObj.subSystemModal = '';
		        	$scope.selectedItem = $scope.selectedBusinessRoleTaskPermList[0];
		        	$scope.selectedBusinessRoleTaskPermList = null;
		        	$scope.openSubsystem();
		        }
	        }
	    };
	    
	    $scope.showBuildInfo = function(){
	    };
	    
	    $scope.loadAdminView = function(){
	    	 windowname = HomeConstantService._ADMIN_SUBSYSTEM_CODE;
		   	 windowparams = HomeConstantService._WIN_PARAMS;
		   	 moveX=moveY=10;
		   	 resizeX=screen.availWidth-10;
		   	 resizeY=screen.availHeight-10;
		   	 windowOpener($location.absUrl().split('#')[0]+"#/"+HomeConstantService._HOME_PARTIAL_PATH+"/"+$scope.userProfile.lanName+"/",windowname,windowparams,moveX,moveY,resizeX,resizeY);    	    	
	    };
	
	    $scope.openSubsystem = function() {
	    	var subSystemWebUrl;
	    	try{
	    		subSystemCode=JSON.parse($scope.selectedItem).subSystem;
		    	selectedRoleCode = JSON.parse($scope.selectedItem).roleCode;
		    	subSystemWebUrl  = JSON.parse($scope.selectedItem).subSystemWebUrl;
	        }catch(e){
	        	subSystemCode = $scope.selectedItem.subSystem;
		    	selectedRoleCode = $scope.selectedItem.roleCode;
		    	subSystemWebUrl = $scope.selectedItem.subSystemWebUrl;
	        }
	    	userLanName =$scope.userProfile.lanName;
	    	
	    	/*for (var property in $scope.subSystemUrls) {
	    		if((subSystemCode + HomeConstantService._SUBSYSTEM_URL_APPENDER) == property){
	    			url =  $scope.subSystemUrls[property];
	    			break;
	    		}
	    	}*/
		   	 windowname = subSystemCode;
		   	 windowparams = HomeConstantService._WIN_PARAMS;
		   	 moveX=moveY=10;
		   	 resizeX=screen.availWidth-10;
		   	 resizeY=screen.availHeight-10;
		   	 windowOpener(subSystemWebUrl,windowname,windowparams,moveX,moveY,resizeX,resizeY); 
		};
		
		$rootScope.menuItemSelectHandler = function(action) {
			//alert("11111"+action);
			if(action == HomeConstantService._SAVE_ACTION){
				if(partial == HomeConstantService._USER_MANGMENT_PARTIAL){
				}else if(partial == HomeConstantService._IPS_ASSIGNMENT_PARTIAL_PATH){
					$rootScope.$broadcast(HomeConstantService._SAVE_ASSIGNMENT_EVENT, []);
				}
			}else if(action == HomeConstantService._IPS_BUILD_INFO_PARTIAL_PATH){
				//alert("11");
				//angular.element('#buildModal').modal('toggle');
				if(!angular.element('#timoutWarningModal').hasClass('in'))angular.element('#timoutWarningModal').modal('toggle');
			}
		};	
		
		var partial = '';
		$scope.getPartial = function () {	        
			switch($rootScope.menuAction){
				case HomeConstantService._USER_MANGMENT_PARTIAL:
					partial = HomeConstantService._USER_MANGMENT_PARTIAL_PATH;
					break;
				case HomeConstantService._IPS_ASSIGNMENT_PARTIAL:					
					partial = HomeConstantService._IPS_ASSIGNMENT_PARTIAL_PATH;	
					break;
				case HomeConstantService._EXIT_PARTIAL:
					window.close();
					partial='';
					return null;
					break;
				case HomeConstantService._HOME_PARTIAL:
					$location.path(HomeConstantService._HOME_PARTIAL_PATH);
					partial='';
					return null;
					break;
				case HomeConstantService._SAVE_ACTION:
					
					break;
			}
	    	return partial;
	    };
	    
	    $scope.logOut = function(logoutType) {
	    	if(closePopupWindows(logoutType)){
	    		window.self.focus();
	    		window.self.moveTo(2,2);
	    		$location.path(HomeConstantService._LOGOUT_PARTIAL);
	    		$rootScope.userProfile = null;
	    	}else{
	    		$scope.extendSession();
	    	}
		};

	    $scope.displayWarning = function(){
	    	$timeout.cancel($scope.sessionTimeout);
	    	$scope.promptWarning();
	    	$scope.sessionTimeout = $timeout($scope.countDown,1);
	    };
	    
		$scope.extendSession = function(){
			if($scope.manualLogoutFlag){
				$scope.manualLogoutFlag = false;
			}else{
				$timeout.cancel($scope.sessionTimeout);
				$scope.counter = HomeConstantService._COUNTDOWN_TIMER_MIN * 60;		
				if(angular.element('#timoutWarningModal').hasClass('in'))angular.element('#timoutWarningModal').modal('toggle');
				setNewTime();
			}
		};
		
		$scope.countDown = function(){
	        $scope.counter--;
	        if($scope.counter >0){
	        	if($scope.counter == 60){
	        		$scope.promptWarning();
	        	}
		        var min = Math.floor($scope.counter / 60);
		        var sec = $scope.counter % 60;
		        $scope.counterMsg = min + ' minutes and ' + sec + ' seconds';
		        $scope.sessionTimeout = $timeout($scope.countDown,1000);
	        }else{
	        	$timeout.cancel($scope.sessionTimeout);
	        	$scope.logOut('notlogout');
	        }
	    };
	    
	    $scope.manualLogout = function(){
	    	$scope.manualLogoutFlag = true;
		};
		
		$scope.promptWarning = function(){
			window.self.focus();
    		window.self.moveTo(2,2);
    		if(!angular.element('#timoutWarningModal').hasClass('in'))angular.element('#timoutWarningModal').modal('toggle');
    		document.getElementById("timeOutWarningSound").src = HomeConstantService._TIMEOUT_WARNING_SOUND;
		};
		
	    if($routeParams.param != null){
	    	$rootScope.loadUserProfile($routeParams.param);
	    }
	    //////////////HOT KEYS ///////////
	    ShortcutKeyService.setKeysForAdminMenu(hotkeys,HomeConstantService);
	}
})();