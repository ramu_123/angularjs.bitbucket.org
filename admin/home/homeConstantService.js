angular.module('IPSAdminApp').constant('HomeConstantService',(function() {
	var _POST_METHOD='POST';
    var _URL_TO_POST='jsonservlet';
    var _HEADER_INFO={'Content-Type': 'application/json'};
    var _ACTION_TYPE_GET_ALL_SUB_SYSTEM={actionType:"GET_ALL_SUB_SYSTEM"};
    var _USER_MANGMENT_PARTIAL = 'showUserManagement';
    var _USER_MANGMENT_PARTIAL_PATH = 'admin/ipsuser/manageUserView.html';    
    var _IPS_ASSIGNMENT_PARTIAL = 'showIPSAssignment';
    var _IPS_ASSIGNMENT_PARTIAL_PATH = "admin/assignment/ipsUserAssignmentView.html";
    var _SAVE_ASSIGNMENT_EVENT = "SAVE_ASSIGNMENT_EVENT";
    
    var _SAVE_ACTION = 'save';
    var _EXIT_PARTIAL = 'exit';
    
    var _HOME_PARTIAL = 'home';
    var _LOGIN_PARTIAL = '/login';
    var _LOGOUT_PARTIAL = '/logout';
    
    var _HOME_PARTIAL_PATH = 'adminHome';
	var _IPS_BUILD_INFO_PARTIAL_PATH = "showBuildInformation";

    var _DEFAULT_COLOR = 'blue';
    var _FONT_SIZE_UNIT = '1.4em';
    var _WIN_PARAMS = "status=no,resizable=yes";
    var _SUBSYSTEM_URL_APPENDER = "_URL";
    var _ADMIN_SUBSYSTEM_CODE = "ADMIN";
    var _ADMIN_APP_TITLE = "IPS Administration";
    var _TIMEOUT_WARNING_SOUND = "admin/assets/audio/beep-1.mp3";
    var _COUNTDOWN_TIMER_MIN = 5;
    
    var _DEFAULT_ROLE_OPTION = {roleCode:"", roleCodeDesc:"Select a Role", subSystem:"", subSystemWebUrl:""};

	/*
	 * Add new subsystems to display in Admin menu
	 * Subsystem code | Subsystem Description | By default enable | Call back function when clicked | View model ID to represent subsystem's view
	 */
    /*var _IPS_SUB_SYSTEMS = [
	                     {subSystemCode:"PREINIT",  subSystemDesc:"IPS National Office Pre Initiation", 	enableDefault:"false",	callBackFn:"",						subSystemModal:"#businessRoleModal"},
	                     {subSystemCode:"POSTINIT", subSystemDesc:"IPS National Office Post Initiation", 	enableDefault:"false",	callBackFn:"",						subSystemModal:"#businessRoleModal"},
	                     {subSystemCode:"FOI", 		subSystemDesc:"IPS Regional/Field Office Initiation", 	enableDefault:"false",	callBackFn:"",						subSystemModal:"#businessRoleModal"},
	                     {subSystemCode:"ppis", 	subSystemDesc:"PPI Sampling", 							enableDefault:"false",	callBackFn:"",						subSystemModal:"#businessRoleModal"},
	                     {subSystemCode:"IXREV", 	subSystemDesc:"IPP Estimation - Index Review", 			enableDefault:"false",	callBackFn:"",						subSystemModal:"#businessRoleModal"},
	                     {subSystemCode:"ESTC", 	subSystemDesc:"IPP Estimation - Calculation", 			enableDefault:"false",	callBackFn:"",						subSystemModal:"#businessRoleModal"},
	                     {subSystemCode:"PUBLICATION",subSystemDesc:"IPP Publication", 						enableDefault:"false",	callBackFn:"",						subSystemModal:"#businessRoleModal"},
	                     {subSystemCode:"ERRM", 	subSystemDesc:"ERRM", 									enableDefault:"false",	callBackFn:"",						subSystemModal:"#businessRoleModal"},
	         {subSystemCode:_ADMIN_SUBSYSTEM_CODE, 	subSystemDesc:"IPS Administration", 					enableDefault:"false",	callBackFn:"",	subSystemModal:"#businessRoleModal"},
	                     {subSystemCode:"BUILD", 	subSystemDesc:"Build Information", 						enableDefault:"true",	callBackFn:"this.showBuildInfo()",	subSystemModal:"#buildModal"}
	         	 ];*/    
	return {
    	_POST_METHOD: _POST_METHOD,
    	_URL_TO_POST: _URL_TO_POST,
    	_HEADER_INFO: _HEADER_INFO,
    	_DEFAULT_COLOR:_DEFAULT_COLOR,
    	_USER_MANGMENT_PARTIAL:_USER_MANGMENT_PARTIAL,
    	_USER_MANGMENT_PARTIAL_PATH:_USER_MANGMENT_PARTIAL_PATH,
    	_IPS_ASSIGNMENT_PARTIAL:_IPS_ASSIGNMENT_PARTIAL,
    	_IPS_ASSIGNMENT_PARTIAL_PATH:_IPS_ASSIGNMENT_PARTIAL_PATH,
    	_EXIT_PARTIAL:_EXIT_PARTIAL,
    	_HOME_PARTIAL:_HOME_PARTIAL,
    	_HOME_PARTIAL_PATH:_HOME_PARTIAL_PATH,
    	_LOGIN_PARTIAL:_LOGIN_PARTIAL,
    	_LOGOUT_PARTIAL:_LOGOUT_PARTIAL,
    	//_IPS_SUB_SYSTEMS:_IPS_SUB_SYSTEMS,
    	_FONT_SIZE_UNIT:_FONT_SIZE_UNIT,
    	_WIN_PARAMS:_WIN_PARAMS,
    	_SUBSYSTEM_URL_APPENDER:_SUBSYSTEM_URL_APPENDER,
    	_ADMIN_SUBSYSTEM_CODE:_ADMIN_SUBSYSTEM_CODE,
    	_ADMIN_APP_TITLE:_ADMIN_APP_TITLE,
    	_TIMEOUT_WARNING_SOUND:_TIMEOUT_WARNING_SOUND,
    	_COUNTDOWN_TIMER_MIN:_COUNTDOWN_TIMER_MIN,
    	_ACTION_TYPE_GET_ALL_SUB_SYSTEM:_ACTION_TYPE_GET_ALL_SUB_SYSTEM,
    	_SAVE_ACTION:_SAVE_ACTION,
    	_SAVE_ASSIGNMENT_EVENT:_SAVE_ASSIGNMENT_EVENT,
    	_DEFAULT_ROLE_OPTION:_DEFAULT_ROLE_OPTION,
		_IPS_BUILD_INFO_PARTIAL_PATH:_IPS_BUILD_INFO_PARTIAL_PATH
    };
})());