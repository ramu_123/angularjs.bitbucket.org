(function (){
  'use strict';
  
angular.module('IPSAdminApp').service('IPSUserAssignmentValueService',IPSUserAssignmentValueService);

function IPSUserAssignmentValueService(){

  var _subsystemList=[];
 
  var _assignmentCategoryList=[];
  
  var _businessRoleList=[];

  var _userList=[];
  
  var _assignmentList=[];
  
  var getBusinessRoleList = function(){
      return _businessRoleList;
  };
  var setBusinessRoleList = function(businessRoleList) {
	  if(angular.isDefined(businessRoleList)==true){	  
		  _businessRoleList = businessRoleList;
	  }
  };

  var getSubsystemList = function(){
      return _subsystemList;
  };
  var setSubsystemList = function(subsystemList) {
	  if(angular.isDefined(subsystemList)==true){	  
		  _subsystemList = subsystemList;
	  }
  };

  var getAssignmentCategoryList = function() {
	  return _assignmentCategoryList;
  };

  var setAssignmentCategoryList = function(assignmentCategoryList) {
	  if(angular.isDefined(assignmentCategoryList)==true){	  
		  _assignmentCategoryList = assignmentCategoryList;
	  }
  };
  
  var getUserList = function() {
	  return _userList;
  };

  var setUserList = function(userList) {
	  if(angular.isDefined(userList)==true){	  
		  _userList = userList;
	  }
  };
  
  var getAssignmentList = function() {
	  return _assignmentList;
  };

  var setAssignmentList = function(assignmentList) {
	  if(angular.isDefined(assignmentList)==true){	  
		  _assignmentList = assignmentList;
	  }
  };
  
  return {
	  	getSubsystemList:getSubsystemList,
	  	setSubsystemList:setSubsystemList,
	  	getAssignmentCategoryList:getAssignmentCategoryList,
	  	setAssignmentCategoryList:setAssignmentCategoryList,
	  	getBusinessRoleList:getBusinessRoleList,
	  	setBusinessRoleList:setBusinessRoleList,
	  	getUserList:getUserList,
	  	setUserList:setUserList,
	  	getAssignmentList:getAssignmentList,
	  	setAssignmentList:setAssignmentList
	   };
};
 
})();