angular.module('IPSAdminApp').constant('IPSUserAssignmentConstantService',(function() {
	var _POST_METHOD='POST';
    var _URL_TO_POST='jsonservlet';
    var _HEADER_INFO={'Content-Type': 'application/json'};
    var _ACTION_TYPE_GET_ASSIGNMENT_CATEGORY={actionType:"GET_ASSIGNMENT_CATEGORY"};
    var _GET_ASSIGNMENT_CATEGORY = "GET_ASSIGNMENT_CATEGORY";
    var _GET_DEFAULT_ASSIGNMENT_CATEGORY = "GET_DEFAULT_ASSIGNMENT_CATEGORY";
    var _ACTION_TYPE_GET_SUB_SYSTEM={actionType:"GET_SUB_SYSTEM"};
    var _GET_SUB_SYSTEM="GET_SUB_SYSTEM";
    var _ACTION_TYPE_GET_BUSINESS_ROLES={actionType:"GET_BUSINESS_ROLES"};
    var _GET_BUSINESS_ROLES="GET_BUSINESS_ROLES";
    var _ACTION_TYPE_GET_USERS={actionType:"GET_USERS"};
    var _GET_USERS="GET_USERS";
    var _ACTION_TYPE_GET_ASSIGNMENT={actionType:"GET_ASSIGNMENT"}; 
    var _GET_ASSIGNMENT="GET_ASSIGNMENT";
    var _GET_DEFAULT_SUB_SYSTEM="GET_DEFAULT_SUB_SYSTEM";
    var _SHOW_EMPTY_MESSAGE = "Under Construction";
    var _ACTION_SAVE_ASSIGNMENT={actionType:"SAVE_ASSIGNMENT"};
    var _SAVE_ASSIGNMENT = "SAVE_ASSIGNMENT";
    var _ASSIGNMENT_DATA = "assignmentData";
    var _TIME_OUT_200_SEC=200*1000;

    var _IPS_ASSIGNMENT_GRID_COL_DEFS=[
                    {field: '_caCode', displayName: 'Checklist Area Code',enableCellEdit: false,width: "20%"},
                    {field: '_caTitle', displayName: 'Checklist Area Title',enableCellEdit: false,width: "47%"},
                    {field: '_sampleCode', displayName: 'Sample#',enableCellEdit: false,width: "10%"},
                    ];
    var _IPS_ASSIGNMENT_GRID_PAGESIZES={ pageSizes: [5,15,30], pageSize: 30, currentPage: 1};    
    var _ASSIGN_TO_IA_USER_CELL_TEMPLATE='<div><select ng-init="preSelectIA(col, row)" ng-model="row.entity._iaName" ng-options="_user._userLanName for _user in user | filter:{_taskCode:\'CA_ASSIGN_IA\'} | orderBy:\'_userLanName\'" ng-change="assignmentChangeHandler(col, row)"><option value=""></option></select></div>';
    var _ASSIGN_TO_SR_USER_CELL_TEMPLATE='<div><select ng-init="preSelectSR(col, row)" ng-model="row.entity._srName" ng-options="_user._userLanName for _user in user | filter:{_taskCode:\'CA_ASSIGN_SR\'} | orderBy:\'_userLanName\'" ng-change="assignmentChangeHandler(col, row)"><option value=""></option></select></div>';
    
    return {
    	_POST_METHOD: _POST_METHOD,
    	_URL_TO_POST: _URL_TO_POST,
    	_HEADER_INFO: _HEADER_INFO,
    	_IPS_ASSIGNMENT_GRID_COL_DEFS:_IPS_ASSIGNMENT_GRID_COL_DEFS,
    	_ACTION_TYPE_GET_ASSIGNMENT_CATEGORY:_ACTION_TYPE_GET_ASSIGNMENT_CATEGORY,
    	_ACTION_TYPE_GET_SUB_SYSTEM:_ACTION_TYPE_GET_SUB_SYSTEM,
    	_GET_ASSIGNMENT_CATEGORY:_GET_ASSIGNMENT_CATEGORY,
    	_GET_SUB_SYSTEM:_GET_SUB_SYSTEM,
    	_GET_DEFAULT_ASSIGNMENT_CATEGORY:_GET_DEFAULT_ASSIGNMENT_CATEGORY,
    	_GET_DEFAULT_SUB_SYSTEM:_GET_DEFAULT_SUB_SYSTEM,
    	_SHOW_EMPTY_MESSAGE:_SHOW_EMPTY_MESSAGE,
    	_ACTION_TYPE_GET_BUSINESS_ROLES:_ACTION_TYPE_GET_BUSINESS_ROLES,
    	_GET_BUSINESS_ROLES:_GET_BUSINESS_ROLES,
    	_ACTION_TYPE_GET_ASSIGNMENT:_ACTION_TYPE_GET_ASSIGNMENT,
    	_GET_ASSIGNMENT:_GET_ASSIGNMENT,
    	_TIME_OUT_200_SEC:_TIME_OUT_200_SEC,
    	_ACTION_TYPE_GET_USERS:_ACTION_TYPE_GET_USERS,
    	_GET_USERS:_GET_USERS,
    	_IPS_ASSIGNMENT_GRID_PAGESIZES:_IPS_ASSIGNMENT_GRID_PAGESIZES,
    	_ASSIGN_TO_IA_USER_CELL_TEMPLATE:_ASSIGN_TO_IA_USER_CELL_TEMPLATE,
    	_ASSIGN_TO_SR_USER_CELL_TEMPLATE:_ASSIGN_TO_SR_USER_CELL_TEMPLATE,
    	_ACTION_SAVE_ASSIGNMENT:_ACTION_SAVE_ASSIGNMENT,
    	_SAVE_ASSIGNMENT:_SAVE_ASSIGNMENT,
    	_ASSIGNMENT_DATA:_ASSIGNMENT_DATA
    };
})());
