(function (){
	'use strict';
	angular.module('IPSAdminApp').controller('IPSUserAssignmentController', IPSUserAssignmentController);
	IPSUserAssignmentController.$inject=['$rootScope','$scope','$http','$q','$filter','IPSUserAssignmentValueService','IPSUserAssignmentConstantService','HomeConstantService'];
	function IPSUserAssignmentController($rootScope,$scope, $http, $q, $filter, IPSUserAssignmentValueService, IPSUserAssignmentConstantService,HomeConstantService) {
		$scope._assignmentCategoryList = [];
		$scope._subsystemList = [];
		$scope._businessRoleList = [];
		$scope._programTypeList = [];
		$scope._userList = [];
		$scope._filteredUserList = [];
		$scope._assignmentList = [];
	 	$scope.pagingOptions= IPSUserAssignmentConstantService._IPS_ASSIGNMENT_GRID_PAGESIZES;
    	$scope.emptyMessage = IPSUserAssignmentConstantService._SHOW_EMPTY_MESSAGE;
    	$scope.showEmptyMessage = false;
    	$scope.dataLoadFlag = false;
    	$scope.changesList = [];
    	$scope.user = [];
    	//Save called
		$scope.$on(HomeConstantService._SAVE_ASSIGNMENT_EVENT, function() {
			if($scope.changesList != null && $scope.changesList.length>0){
				IPSUserAssignmentConstantService._ACTION_SAVE_ASSIGNMENT[IPSUserAssignmentConstantService._ASSIGNMENT_DATA] = $scope.changesList;
				$http(
			   	     	 {
			   	            method : IPSUserAssignmentConstantService._POST_METHOD,
			   	            headers: IPSUserAssignmentConstantService._HEADER_INFO,
			   	            url : IPSUserAssignmentConstantService._URL_TO_POST,
			   	            data: IPSUserAssignmentConstantService._ACTION_SAVE_ASSIGNMENT
			   	          }
			   	      ).success(function(responseData) {
			   	    	alert("Data saved successfully!");
			   	    	$scope.changesList = [];
			   	         }
			   	      ).error(function(data, status, headers, config) { alert('Network Error, Code:' + status);});
				
			}else{
				alert("No changes to save!");
			}
    	});    	
    	angular.forEach($rootScope.userProfile.programTypeCode.split("/"), function(programCode){
    		$scope._programTypeList.push({name:programCode});
        });
    	$scope.showProgramType = $scope._programTypeList.length > 1;
    	var assignmentCategoryDeferred = $q.defer();
    	$http(
   	     	 {
   	            method : IPSUserAssignmentConstantService._POST_METHOD,
   	            headers: IPSUserAssignmentConstantService._HEADER_INFO,
   	            url : IPSUserAssignmentConstantService._URL_TO_POST,
   	            data: IPSUserAssignmentConstantService._ACTION_TYPE_GET_ASSIGNMENT_CATEGORY
   	          }
   	      ).success(function(responseData) {
   	    	assignmentCategoryDeferred.resolve(responseData);
   	         }
   	      ).error(function(data, status, headers, config) { assignmentCategoryDeferred.reject('Network Error, Code:' + status);});
    	$scope.assignmentCategoryPromise = assignmentCategoryDeferred.promise;
    	$scope.assignmentCategoryPromise.then(
	    		function(resolve){
	    			setTimeout(function () {
    				    $scope.$apply(function () {
    			   	    	  $scope.setAssignmentCategoryList(resolve[IPSUserAssignmentConstantService._GET_ASSIGNMENT_CATEGORY]);
    			   	    	  $scope._assignmentCategoryList = resolve[IPSUserAssignmentConstantService._GET_ASSIGNMENT_CATEGORY];
    			   	    	  $scope.assignmentCategory = $scope.getDefaultAssignmentCategory();
    				    });
    				}, 1);
	            }, 
	            function(reject){
	            	alert(reject);
	            }
	        );
    	var subsystemDeferred = $q.defer();
    	$http(
  	     	 {
  	            method : IPSUserAssignmentConstantService._POST_METHOD,
  	            headers: IPSUserAssignmentConstantService._HEADER_INFO,
  	            url : IPSUserAssignmentConstantService._URL_TO_POST,
  	            data: IPSUserAssignmentConstantService._ACTION_TYPE_GET_SUB_SYSTEM
  	          }
  	      ).success(function(responseData) {
  	    	subsystemDeferred.resolve(responseData);
  	         }
  	      ).error(function(data, status, headers, config) {subsystemDeferred.reject('Network Error, Code:' + status); });
    	$scope.subsystemPromise = subsystemDeferred.promise;
    	$scope.subsystemPromise.then(
	    		function(resolve){
	    			setTimeout(function () {
    				    $scope.$apply(function () {
    			  	          $scope.setSubsystemList(resolve[IPSUserAssignmentConstantService._GET_SUB_SYSTEM]);
    			  	          $scope._subsystemList = resolve[IPSUserAssignmentConstantService._GET_SUB_SYSTEM];
    			  	    	  $scope.subSystem = $scope.getDefaultSubsystem();
    			  	    	  $scope.getBusinessRoles();
    				    });
    				}, 1);
	            }, 
	            function(reject){
	            	alert(reject);
	            }
	        );
    	$scope.getBusinessRoles = function(){
    		$http(
        	     	 {
        	            method : IPSUserAssignmentConstantService._POST_METHOD,
        	            headers: IPSUserAssignmentConstantService._HEADER_INFO,
        	            url : IPSUserAssignmentConstantService._URL_TO_POST,
        	            data: IPSUserAssignmentConstantService._ACTION_TYPE_GET_BUSINESS_ROLES
        	          }
        	      ).success(function(responseData) {
        	          $scope.setBusinessRoleList(responseData[IPSUserAssignmentConstantService._GET_BUSINESS_ROLES]);
        	          $scope._businessRoleList = responseData[IPSUserAssignmentConstantService._GET_BUSINESS_ROLES];
        	          $scope.businessRole =  $scope._businessRoleList;
        	          $scope.getUsers();
        	         }
        	      ).error(function(data, status, headers, config) { });
    	};
    	
	   	$scope.getDefaultAssignmentCategory = function(){
			var returnValue;
			angular.forEach(IPSUserAssignmentValueService.getAssignmentCategoryList(), function(assignCat){
				if(assignCat._businessObjectCode == "CA"){
					returnValue = assignCat;
				}
	    	});
			return returnValue;
		};
		
   		$scope.getDefaultSubsystem = function(){
    		var returnValue;
    		angular.forEach(IPSUserAssignmentValueService.getSubsystemList(), function(subSys){
    			if(subSys._subsystemCode == "PREINIT"){
    				returnValue = subSys;
    			}
        	});
    		return returnValue;
    	};
    	
    	$scope.assignmentCategoryChangeHandler = function(){
    		$scope.validate();
    	};
    	
    	$scope.subsystemChangeHandler = function(){
    		$scope.validate();
    	};
    	
    	$scope.businessRoleChangeHandler = function(){
    		$scope.filterUsersByBusinessRole();
    		$scope.determineColDefs(); 
    		$scope.user = [];
    	};
    	$scope.determineColDefs = function(){
    		var _TEMP_IPS_SUBSYSTEM_GRID_COL_DEFS = [];
    		_TEMP_IPS_SUBSYSTEM_GRID_COL_DEFS.push.apply(_TEMP_IPS_SUBSYSTEM_GRID_COL_DEFS, IPSUserAssignmentConstantService._IPS_ASSIGNMENT_GRID_COL_DEFS);
    		angular.forEach($scope.businessRole, function(bRole){
    			var _cellTemplate = null;
    			if(bRole.businessRoleCode == 'IA'){
    				_cellTemplate = IPSUserAssignmentConstantService._ASSIGN_TO_IA_USER_CELL_TEMPLATE;
    			}else if(bRole.businessRoleCode == 'SR'){
    				_cellTemplate = IPSUserAssignmentConstantService._ASSIGN_TO_SR_USER_CELL_TEMPLATE;
    			}
    			_TEMP_IPS_SUBSYSTEM_GRID_COL_DEFS.push({field: '_'+angular.lowercase(bRole.businessRoleCode) + 'Name', displayName: bRole.businessRoleCode ,enableCellEdit: false,width: "**", cellTemplate:_cellTemplate});
	    	});
    		$scope.assignmentColDefs = _TEMP_IPS_SUBSYSTEM_GRID_COL_DEFS;
    	};
    	$scope.validate = function(){
    		var assignmentCatFlag =  false;
    		var subSystemFlag = false;
    		if($scope.assignmentCategory){
	    		if (!angular.isArray($scope.assignmentCategory)) {
	    			if($scope.assignmentCategory._businessObjectCode == "CA"){
	    				assignmentCatFlag = true;
	    			}
	    		}else{
		    		angular.forEach($scope.assignmentCategory, function(assignCat){
		    			if(assignCat._businessObjectCode == "CA"){
		    				assignmentCatFlag = true;
						}
			    	});
	    		}
    		}
    		if($scope.subSystem){
    			if (!angular.isArray($scope.subSystem)) {
    				if($scope.subSystem._subsystemCode == "PREINIT"){
    					subSystemFlag = true;
        			}
    			}else{
	    			angular.forEach($scope.subSystem, function(subSys){
	        			if(subSys._subsystemCode == "PREINIT"){
	        				subSystemFlag = true;
	        			}
	            	});
    			}
    		}
    		$scope.showEmptyMessage = !(assignmentCatFlag && subSystemFlag);
    	};
    	
    	$scope.setAssignmentCategoryList = function(assignmentCategoryList){
    		IPSUserAssignmentValueService.setAssignmentCategoryList(assignmentCategoryList);
	    };
	    
    	$scope.setSubsystemList = function(subSystemList){
    		IPSUserAssignmentValueService.setSubsystemList(subSystemList);
	    };
	    
    	$scope.setBusinessRoleList = function(businessRoleList){
    		IPSUserAssignmentValueService.setBusinessRoleList(businessRoleList);
	    };
	    
    	$scope.ipsAssignmentList = [];
		$scope.assignmentColDefs = IPSUserAssignmentConstantService._IPS_ASSIGNMENT_GRID_COL_DEFS;
		$scope.assignmentGridOptions = { 
	              data: '_assignmentList',
	              columnDefs:'assignmentColDefs',
	              pagingOptions:$scope.pagingOptions,
	              enableColumnResize: false,
	              displaySelectionCheckbox: false,
	              enableCellSelection: true,
	              canSelectRows: false,
	              enablePaging: true,
	              cellClass: 'wrap-text',
	              multiSelect:false,
	              showFilter:false,
	      		  showFooter: true
		};
		$scope.getUsers = function(){
			var usersDeferred = $q.defer();
			$http(
		   	     	 {
		   	            method : IPSUserAssignmentConstantService._POST_METHOD,
		   	            headers: IPSUserAssignmentConstantService._HEADER_INFO,
		   	            url : IPSUserAssignmentConstantService._URL_TO_POST,
		   	            data: IPSUserAssignmentConstantService._ACTION_TYPE_GET_USERS
		   	          }
		   	      ).success(function(responseData) {
		   	    	usersDeferred.resolve(responseData);
		   	         }
		   	      ).error(function(data, status, headers, config) { usersDeferred.reject('Network Error, Code:' + status);});
			$scope.usersPromise = usersDeferred.promise;
	    	$scope.usersPromise.then(
		    		function(resolve){
		    			setTimeout(function () {
	    				    $scope.$apply(function () {
	    				    	$scope.setUserList(resolve[IPSUserAssignmentConstantService._GET_USERS]);
	    			   	    	  $scope._userList = resolve[IPSUserAssignmentConstantService._GET_USERS];
	    			   	    	  $scope.filterUsersByBusinessRole();
	    			   	    	  $scope.determineColDefs(); 
	    				    });
	    				}, 1);
		            }, 
		            function(reject){
		            	alert(reject);
		            }
		        );
		};
    	$scope.setUserList = function(userList){
    		IPSUserAssignmentValueService.setUserList(userList);
	    };
		
    	$scope.filterUsersByBusinessRole = function(){
    		$scope._filteredUserList= [];
    		angular.forEach($scope.businessRole, function(bRole){            
    			$scope._filteredUserList.push.apply($scope._filteredUserList, $filter('filter')($scope._userList, { _businessRoleCode: bRole.businessRoleCode }));
    		});  
    		$scope._filteredUserList = $scope.uniqueList($scope._filteredUserList, '_userLanName');
    		$scope._filteredUserList = $filter('orderBy')($scope._filteredUserList, '_userLanName');
    		if (!$scope.$$phase) {
	        	$scope.$apply();
	        }
	    };
	    $scope.uniqueList = function(list, field){
	    	var uniqueList = {};
	    	if(list != null && field != null){
	    		angular.forEach(list, function(item){  
	    			uniqueList[item[field]] = item;
	    		});
	    	}
	    	return Object.keys(uniqueList).map(function(k) { return uniqueList[k]; });
	    };
		$scope.showAssignments = function(_currentPage, _pageSize){
			var deferred = $q.defer();
			$scope.dataLoadFlag = true;
			$scope._assignmentList = [];
			IPSUserAssignmentConstantService._ACTION_TYPE_GET_ASSIGNMENT['currentPage']=_currentPage;
			IPSUserAssignmentConstantService._ACTION_TYPE_GET_ASSIGNMENT['pageSize']=_pageSize;
			$http(
		   	     	 {
		   	            method : IPSUserAssignmentConstantService._POST_METHOD,
		   	            headers: IPSUserAssignmentConstantService._HEADER_INFO,
		   	            url : IPSUserAssignmentConstantService._URL_TO_POST,
		   	            data: IPSUserAssignmentConstantService._ACTION_TYPE_GET_ASSIGNMENT
		   	          }
		   	      ).success(function(responseData) {
		   	    	  deferred.resolve(responseData);
		   	         }
		   	      ).error(function(data, status, headers, config) { deferred.reject('Network Error, Code:' + status);});
			$scope.assignmentPromise = deferred.promise;
			$scope.assignmentPromise.then(
		    		function(resolve){
		    			$scope._assignmentList = angular.fromJson(resolve[IPSUserAssignmentConstantService._GET_ASSIGNMENT]);
		    			if (!$scope.$$phase) {
		    	        	$scope.$apply();
		    	        }
		            }, 
		            function(reject){
		            	alert(reject);
		            }
		        );
		};
		$scope.assignmentChangeHandler = function(col,row) {
			$scope.changesList.push.apply($scope.changesList, $scope.getAssignmentValue(row.entity));
	    };		
	    $scope.preSelectIA = function(col,row) {
	    	row.entity._iaName = $filter('filter')($scope._userList, { _userSid: row.entity._iaUserSid, _taskCode:'CA_ASSIGN_IA' })[0];
	    };
	    $scope.preSelectSR = function(col,row) {
	    	row.entity._srName = $filter('filter')($scope._userList, { _userSid: row.entity._srUserSid, _taskCode:'CA_ASSIGN_SR' })[0];
	    };
	    $scope.getAssignmentValue = function(entity) {
	    	var returnList = [];
	    	angular.forEach($scope.businessRole, function(bRole){
	    		if(entity["_"+angular.lowercase(bRole.businessRoleCode) + 'Name'] !=  null){
    				returnList.push({_businessObjectSid:entity._businessObjectSid, 
  						 _businessObjectCode:$scope.assignmentCategory._businessObjectCode,
  						 _subsystemCode:$scope.subSystem._subsystemCode,
  						 _assignedToUserSid:entity["_"+angular.lowercase(bRole.businessRoleCode) + 'Name']["_userSid"],
  						 _assignedToRoleCode:entity["_"+angular.lowercase(bRole.businessRoleCode) + 'Name']["_businessRoleCode"],
  						 _assignedToTaskCode:entity["_"+angular.lowercase(bRole.businessRoleCode) + 'Name']["_taskCode"],
  						 _assignedByUserSid:$rootScope.userProfile.userSid
	    			});
	    		}else if(entity["_"+angular.lowercase(bRole.businessRoleCode) + 'UserSid'] != null ){
	    				returnList.push({_businessObjectSid:entity._businessObjectSid, 
	   						 _businessObjectCode:$scope.assignmentCategory._businessObjectCode,
	   						 _subsystemCode:$scope.subSystem._subsystemCode,
	   						 _assignedToUserSid:null,
	   						 _assignedToRoleCode:bRole.businessRoleCode,
	   						 _assignedToTaskCode:bRole.taskCode,
	   						 _assignedByUserSid:$rootScope.userProfile.userSid
		    			});
	    		}
	    	});
	    	return returnList;
	    };
	    /*$scope.$watch(
			    function () {
			        return {
			            currentPage: $scope.pagingOptions.currentPage,
			            pageSize: $scope.pagingOptions.pageSize
			        };
			    },
			    function (newVal, oldVal) {
			        if (newVal.pageSize !== oldVal.pageSize) {
			            $scope.pagingOptions.currentPage = 1;
			        }
			        if($scope.dataLoadFlag)$scope.showAssignments($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);
			    },
			true);*/		
	};
})();