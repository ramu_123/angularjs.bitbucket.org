angular.module("IPSAdminApp", ['ngRoute','ui.bootstrap','headerApp','menuApp','ngGrid','ui-util','ngAnimate','cgBusy','ui.multiselect','cfp.hotkeys']).config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/login', {
        templateUrl: "admin/login/loginView.html",title: 'IPS Home - Login'
      }).
      when('/login/:pathName/:lanName', {
          templateUrl: 'admin/login/loginView.html',title: 'IPS Home - Login'
      }).
      when('/home', {
          templateUrl: 'admin/home/ipsHomeView.html',title: 'IPS Home - Home'
      }).
      when('/adminHome', {
          templateUrl: 'admin/home/adminHomeView.html',title: 'IPS Administration'
      }).
      when('/logout', {
          templateUrl: 'admin/home/logOutView.html',title: 'IPS Home - Logout'
      }).
      when('/GateWay/:pathName', {
          templateUrl: 'admin/login/loginView.html',title: 'IPS Home - Login'
      }).
      when('/SSOGateWay/:ssoToken', {
          templateUrl: 'admin/login/loginView.html',title: 'IPS Home - Login'
      }).
      when('/error', {
          templateUrl: 'admin/home/errorView.html',title: 'IPS Home - Error'
      }).
      otherwise({
        redirectTo: '/login'
      });
  }]);
